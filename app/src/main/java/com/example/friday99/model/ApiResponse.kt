package com.example.friday99.model

data class ApiResponse(
    val courses: List<Course>,
    val topic: List<Topic>
)