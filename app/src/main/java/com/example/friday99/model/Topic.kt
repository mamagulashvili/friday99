package com.example.friday99.model

data class Topic(
    val color: String,
    val duration: Int,
    val title: String,
    val type: String
)