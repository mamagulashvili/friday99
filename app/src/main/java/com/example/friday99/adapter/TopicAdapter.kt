package com.example.friday99.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.friday99.R
import com.example.friday99.databinding.RowTopicItemBinding
import com.example.friday99.model.Topic

class TopicAdapter : RecyclerView.Adapter<TopicAdapter.TopicViewHolder>() {
    companion object {
        private const val TYPE_FREE = "free"
    }

    inner class TopicViewHolder(val binding: RowTopicItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var model: Topic

        fun onBind() {
            model = differ.currentList[adapterPosition]
            binding.apply {
                tvDuration.text = "${model.duration} Min"
                tvTitle.text = model.title
            }
            binding.root.setCardBackgroundColor(Color.parseColor("#${model.color}"))
            if (model.type == TYPE_FREE)
                binding.ivPaidIcon.setBackgroundResource(R.drawable.ic_wallet)
            else
                binding.ivPaidIcon.setImageResource(R.drawable.ic_setting)
        }
    }

    val diffUtil = object : DiffUtil.ItemCallback<Topic>() {
        override fun areItemsTheSame(oldItem: Topic, newItem: Topic): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: Topic, newItem: Topic): Boolean {
            return oldItem == newItem
        }


    }
    val differ = AsyncListDiffer(this, diffUtil)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopicViewHolder =
        TopicViewHolder(
            RowTopicItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

    override fun onBindViewHolder(holder: TopicViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int = differ.currentList.size
}