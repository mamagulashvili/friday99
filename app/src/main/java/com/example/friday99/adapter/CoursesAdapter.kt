package com.example.friday99.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.friday99.databinding.RowCoursesItemBinding
import com.example.friday99.extensions.getImage
import com.example.friday99.model.Course
import kotlin.math.roundToInt

class CoursesAdapter : RecyclerView.Adapter<CoursesAdapter.CoursesViewHolder>() {
    inner class CoursesViewHolder(private val binding: RowCoursesItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var model: Course
        fun onBind() {
            model = differ.currentList[adapterPosition]
            binding.apply {
                val percent = (model.background_color_precent.toInt()) / 100.toFloat()
                root.setCardBackgroundColor(
                    getColorWithAlpha(
                        Color.parseColor("#${model.color}"),
                        percent
                    )
                )
                tvTitleSecond.text = model.title
                tvTitleSecond.setTextColor(getColorWithAlpha(Color.WHITE, percent))
                ivCardImage.getImage(model.image)
                tvTitle.text = model.title
                progressBar.progress = model.precent.toFloat().toInt() * 100
                cardView.setBackgroundColor(Color.parseColor("#${model.color}"))
            }
        }

        private fun getColorWithAlpha(color: Int, ratio: Float): Int {
            return Color.argb(
                (Color.alpha(color) * ratio).roundToInt(),
                Color.red(color),
                Color.green(color),
                Color.blue(color)
            )
        }
    }

    private val diffUtil = object : DiffUtil.ItemCallback<Course>() {
        override fun areItemsTheSame(oldItem: Course, newItem: Course): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: Course, newItem: Course): Boolean {
            return oldItem == newItem
        }
    }
    val differ = AsyncListDiffer(this, diffUtil)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoursesViewHolder =
        CoursesViewHolder(
            RowCoursesItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

    override fun onBindViewHolder(holder: CoursesViewHolder, position: Int) = holder.onBind()

    override fun getItemCount(): Int = differ.currentList.size

}