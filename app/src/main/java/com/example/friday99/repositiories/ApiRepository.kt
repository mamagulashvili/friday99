package com.example.friday99.repositiories

import com.example.friday99.model.ApiResponse
import com.example.friday99.util.Resource

interface ApiRepository {

    suspend fun getApiResponse():Resource<ApiResponse>
}