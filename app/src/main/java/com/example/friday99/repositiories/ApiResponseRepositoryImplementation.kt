package com.example.friday99.repositiories

import com.example.friday99.model.ApiResponse
import com.example.friday99.network.ApiService
import com.example.friday99.util.Resource
import javax.inject.Inject

class ApiResponseRepositoryImplementation @Inject constructor(
    private val api: ApiService
) : ApiRepository {
    override suspend fun getApiResponse(): Resource<ApiResponse> {
        return try {
            val response = api.getCources()
            if (response.isSuccessful) {
                Resource.Success(response.body()!!)
            } else {
                Resource.Error(response.errorBody().toString())
            }
        } catch (e: Exception) {
            Resource.Error(e.toString())
        }
    }
}