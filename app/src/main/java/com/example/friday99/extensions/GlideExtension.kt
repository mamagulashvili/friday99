package com.example.friday99.extensions

import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import com.example.friday99.R

fun AppCompatImageView.getImage(imageUrl: String) {
    Glide.with(this.context).load(imageUrl).placeholder(R.mipmap.ic_launcher_round).into(this)
}