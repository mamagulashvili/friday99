package com.example.friday99.network

import com.example.friday99.model.ApiResponse
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {
    @GET("/v3/29db8caa-95cb-44be-aa3c-eee0aa406870")
    suspend fun getCources():Response<ApiResponse>
}