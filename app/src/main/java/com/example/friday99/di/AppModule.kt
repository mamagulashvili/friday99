package com.example.friday99.di

import com.example.friday99.network.ApiService
import com.example.friday99.repositiories.ApiRepository
import com.example.friday99.repositiories.ApiResponseRepositoryImplementation
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    private const val BASE_URL = "https://run.mocky.io/"
    @Singleton
    @Provides
    fun proveApiService(): ApiService = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ApiService::class.java)
    @Provides
    @Singleton
    fun provideRepository(authService: ApiService): ApiRepository =
        ApiResponseRepositoryImplementation(authService)
}