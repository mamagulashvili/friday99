package com.example.friday99.fragment

import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.friday99.adapter.CoursesAdapter
import com.example.friday99.adapter.TopicAdapter
import com.example.friday99.databinding.MainFragmentBinding
import com.example.friday99.util.Resource
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainFragment : Fragment() {

    private var _binding: MainFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MainViewModel by viewModels()
    private val topicAdapter: TopicAdapter by lazy { TopicAdapter() }
    private val coursesAdapter: CoursesAdapter by lazy { CoursesAdapter() }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = MainFragmentBinding.inflate(layoutInflater)
        init()
        return binding.root
    }

    private fun init() {
        lifecycleScope.launch {
            viewModel.getResponse()
        }
        observeResponse()
        initRecycleViews()
    }

    private fun observeResponse() {
        viewModel.apiResponse.observe(viewLifecycleOwner, {
            when (it) {
                is Resource.Loading -> {

                }
                is Resource.Success -> {
                    topicAdapter.differ.submitList(it.data?.topic)
                    coursesAdapter.differ.submitList(it.data?.courses)
                }
                is Resource.Error -> {
                    d("Error", "${it.errorMessage}")
                }
            }
        })
    }

    private fun initRecycleViews() {
        binding.rvTopics.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            adapter = topicAdapter
        }
        binding.rvCourses.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = coursesAdapter
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}