package com.example.friday99.fragment

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.friday99.model.ApiResponse
import com.example.friday99.repositiories.ApiResponseRepositoryImplementation
import com.example.friday99.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repo: ApiResponseRepositoryImplementation
) : ViewModel() {

    val apiResponse: MutableLiveData<Resource<ApiResponse>> = MutableLiveData()

    fun getResponse() = viewModelScope.launch {
        apiResponse.postValue(Resource.Loading())
        withContext(Dispatchers.IO) {
            val result = repo.getApiResponse()
            apiResponse.postValue(result)
        }
    }
}